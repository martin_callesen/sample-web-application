package dk.testhuset.web;

import dk.testhuset.web.page.QuotePage;
import org.junit.Test;

import static dk.testhuset.web.page.QuotePage.requestQuotePage;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;

public class QuotePageComponentTest extends PageTest {
    private static final String QUOTE_URL = "http://sample-api:8080/api/quote";
    private static final String QUOTE = "I am a berliner";
    private static final String QUOTE_SUCCESS = "{\"quote\":\""+QUOTE+"\"}";

    @Test
    public void showQuotePageShown() throws Exception {
        mockServiceAt(QUOTE_URL).toRespondWith(QUOTE_SUCCESS);
        QuotePage quotePage = requestQuotePage(this);
        assertThat("QuotePage", quotePage, is(notNullValue()));
    }

    @Test
    public void showQuotePageContainQuote() throws Exception {
        mockServiceAt(QUOTE_URL).toRespondWith(QUOTE_SUCCESS);
        QuotePage quotePage = requestQuotePage(this);
        assertThat("Quote on page", quotePage.getQuote(), is(QUOTE));
    }

    @Test
    public void showQuotePageContainDescriptiveText() throws Exception {
        mockServiceAt(QUOTE_URL).toRespondWith(QUOTE_SUCCESS);
        QuotePage quotePage = requestQuotePage(this);
        assertThat("Header on page", quotePage.getHeader(), is("Quote"));
    }

    @Test
    public void showQuotePageContainButtonForNewQuote() throws Exception {
        mockServiceAt(QUOTE_URL).toRespondWith(QUOTE_SUCCESS);
        QuotePage quotePage = requestQuotePage(this);
        assertThat("Button on page", quotePage.getButtonText(), is("Refresh"));
    }
}
