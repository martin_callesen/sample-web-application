package dk.testhuset.web;

import dk.testhuset.WebApplication;
import dk.testhuset.api.mock.ApiMockServer;
import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.WebApplicationContext;

import static dk.testhuset.api.mock.ApiMockServer.createApiMockServer;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(WebApplication.class)
@WebIntegrationTest
@ActiveProfiles({"ci"})
public abstract class PageTest {
    private static final String CONTENT_TYPE = "text/html;charset=UTF-8";

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private WebApplicationContext wac;

    private ApiMockServer apiMockServer;
    private MockMvc mockMvc;

    @Before
    public final void setUpMockMvc() throws Exception {
        this.mockMvc = webAppContextSetup(this.wac).build();
    }

    @Before
    public final void setUpApiMockServer() throws Exception {
        this.apiMockServer = createApiMockServer(this.restTemplate);
    }

    @After
    public final void tearDownApiMockServer() throws Exception {
        this.apiMockServer.verify();
    }

    public final MvcResult requestToPage(String urlTemplate) throws Exception {
        return this.mockMvc.perform(get(urlTemplate).accept(MediaType.TEXT_HTML))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(CONTENT_TYPE))
                    .andReturn();
    }

    public final ApiMockServer mockServiceAt(String url) {
        return this.apiMockServer.mockServiceAt(url);
    }
}
