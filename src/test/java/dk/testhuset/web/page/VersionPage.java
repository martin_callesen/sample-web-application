package dk.testhuset.web.page;

import dk.testhuset.web.PageTest;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.test.web.servlet.MvcResult;

import java.io.UnsupportedEncodingException;

import static dk.testhuset.web.controller.VersionController.VERSION_URL;

public final class VersionPage {
    private static final String VERSION_FIELD = "versionField";
    private static final String API_VERSION_FIELD = "apiVersionField";
    private static final String DATABASE_VERSION_FIELD = "databaseVersionField";
    private final Document html;

    private VersionPage(MvcResult mvcResult) throws UnsupportedEncodingException {
        this.html = HTMLPage.parse(mvcResult);
    }

    public static VersionPage requestVersionPage(PageTest pageTest) throws Exception {
        MvcResult mvcResult = pageTest.requestToPage(VERSION_URL);

        return new VersionPage(mvcResult);
    }

    public String getVersion() {
        Element quoteElement = this.html.getElementById(VERSION_FIELD);

        return quoteElement.text();
    }

    public String getApiVersion() {
        Element quoteElement = this.html.getElementById(API_VERSION_FIELD);

        return quoteElement.text();
    }

    public String getDatabaseVersion() {
        Element quoteElement = this.html.getElementById(DATABASE_VERSION_FIELD);

        return quoteElement.text();
    }
}
