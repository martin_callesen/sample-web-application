package dk.testhuset.web.page;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MvcResult;

import java.io.UnsupportedEncodingException;

public final class HTMLPage {
    private HTMLPage() {}

    public static Document parse(MvcResult mvcResult) throws UnsupportedEncodingException {
        MockHttpServletResponse response = mvcResult.getResponse();
        String contentAsString = response.getContentAsString();

        return Jsoup.parse(contentAsString);
    }
}
