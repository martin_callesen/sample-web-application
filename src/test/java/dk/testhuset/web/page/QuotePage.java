package dk.testhuset.web.page;

import dk.testhuset.web.PageTest;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MvcResult;

import java.io.UnsupportedEncodingException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;

public class QuotePage {
    private final Document html;

    private QuotePage(MvcResult result) throws UnsupportedEncodingException {
        MockHttpServletResponse response = result.getResponse();
        String contentAsString = response.getContentAsString();
        this.html = Jsoup.parse(contentAsString);
    }

    public static QuotePage requestQuotePage(PageTest pageComponentTest) throws Exception {
        MvcResult mvcResult = pageComponentTest.requestToPage("/quote");

        return new QuotePage(mvcResult);
    }

    public String getHeader() {
        Elements headerElement = this.html.getElementsByClass("panel-heading");

        return headerElement.text();
    }

    public String getQuote() {
        Element quoteElement = this.html.getElementById("quoteField");

        return quoteElement.text();
    }

    public String getButtonText() {
        Elements buttonElement = this.html.getElementsByClass("btn");

        return buttonElement.text();
    }
}
