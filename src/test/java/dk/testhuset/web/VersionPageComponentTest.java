package dk.testhuset.web;

import dk.testhuset.web.domain.WebVersion;
import dk.testhuset.web.page.VersionPage;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.mockito.Mockito.when;

public class VersionPageComponentTest extends PageTest{
    private static final String VERSION_URL = "http://sample-api:8080/api/version";
    private static final String API_VERSION = "1.2.11";
    private static final String DATABASE_VERSION = "201509231252";
    private static final String VERSION_SUCCESS = "{\"applicationVersion\":\"" + API_VERSION + "\",\"databaseVersion\":\"" + DATABASE_VERSION + "\"}";

    @Autowired
    private WebVersion webVersion;

    @Test
    public void versionPageShown() throws Exception {
        mockServiceAt(VERSION_URL).toRespondWith(VERSION_SUCCESS);
        VersionPage versionPage = VersionPage.requestVersionPage(this);
        assertThat("VersionPage", versionPage, is(notNullValue()));
    }

    @Test
    public void webVersionNumberShownOnPage() throws Exception {
        String version = "1.0.10";
        mockServiceAt(VERSION_URL).toRespondWith(VERSION_SUCCESS);
        when(this.webVersion.getApplicationVersion()).thenReturn(version);
        VersionPage versionPage = VersionPage.requestVersionPage(this);
        assertThat("VersionPage - Web Version", versionPage.getVersion(), is(version));
    }

    @Test
    public void apiVersionNumberShownOnPage() throws Exception {
        mockServiceAt(VERSION_URL).toRespondWith(VERSION_SUCCESS);
        VersionPage versionPage = VersionPage.requestVersionPage(this);
        assertThat("VersionPage - API Version", versionPage.getApiVersion(), is(API_VERSION));
    }

    @Test
    public void databaseVersionNumberShownOnPage() throws Exception {
        mockServiceAt(VERSION_URL).toRespondWith(VERSION_SUCCESS);
        VersionPage versionPage = VersionPage.requestVersionPage(this);
        assertThat("VersionPage - Database Version", versionPage.getDatabaseVersion(), is(DATABASE_VERSION));
    }
}
