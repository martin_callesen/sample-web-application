package dk.testhuset.api.mock;

import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.test.web.client.ResponseActions;
import org.springframework.test.web.client.match.MockRestRequestMatchers;
import org.springframework.test.web.client.response.MockRestResponseCreators;
import org.springframework.web.client.RestTemplate;

import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;

public final class ApiMockServer {
    public MockRestServiceServer mockRestServiceServer;
    private ResponseActions responseActions;

    private ApiMockServer() {
    }

    public static ApiMockServer createApiMockServer(RestTemplate restTemplate) {
        ApiMockServer apiMockServer = new ApiMockServer();
        apiMockServer.mockRestServiceServer = MockRestServiceServer.createServer(restTemplate);

        return apiMockServer;
    }

    public void toRespondWith(String response) {
        responseActions.andRespond(MockRestResponseCreators.withSuccess(response, MediaType.APPLICATION_JSON));
    }

    public ApiMockServer mockServiceAt(String serviceUrl) {
        this.responseActions = this.mockRestServiceServer.expect(
                requestTo(serviceUrl))
                .andExpect(MockRestRequestMatchers.method(HttpMethod.GET));
        return this;
    }

    public void verify() {
        this.mockRestServiceServer.verify();
    }
}