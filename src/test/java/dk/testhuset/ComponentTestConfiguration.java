package dk.testhuset;

import dk.testhuset.web.domain.WebVersion;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import static org.mockito.Mockito.mock;

@Configuration
@Profile("ci")
public class ComponentTestConfiguration {
    @Bean
    public WebVersion webVersion() {
        return mock(WebVersion.class);
    }
}
