package dk.testhuset.web.controller;


import dk.samples.api.dto.VersionRestResponse;
import dk.testhuset.api.client.ApiVersionClient;
import dk.testhuset.web.domain.WebVersion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class VersionController {
    public static final String VERSION_TEXT_ATTRIBUTE_NAME = "versionText";
    public static final String API_VERSION_TEXT_ATTRIBUTE_NAME = "apiVersionText";
    public static final String DATABASE_VERSION_TEXT_ATTRIBUTE_NAME = "databaseVersionText";
    public static final String VERSION_URL = "/version";

    @Autowired
    private WebVersion webVersion;

    @Autowired
    private ApiVersionClient versionClient;

    @RequestMapping(VERSION_URL)
    String version(Model model) {
        String versionText = this.webVersion.getApplicationVersion();
        VersionRestResponse apiVersion = this.versionClient.loadApiVersion();
        String apiVersionText = apiVersion.getApplicationVersion();
        String databaseVersionText = apiVersion.getDatabaseVersion();
        model.addAttribute(VERSION_TEXT_ATTRIBUTE_NAME, versionText);
        model.addAttribute(API_VERSION_TEXT_ATTRIBUTE_NAME, apiVersionText);
        model.addAttribute(DATABASE_VERSION_TEXT_ATTRIBUTE_NAME, databaseVersionText);

        return "version";
    }
}
