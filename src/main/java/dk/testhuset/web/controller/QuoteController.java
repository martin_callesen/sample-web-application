package dk.testhuset.web.controller;


import dk.testhuset.api.client.ApiQuoteClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class QuoteController {
    public static final String QUOTE_TEXT_ATTRIBUTE_NAME = "quoteText";

    @Autowired
    private ApiQuoteClient apiQuoteClient;

    @RequestMapping("/quote")
    String quote(Model model) {
        String quoteText = this.apiQuoteClient.getQuote();
        model.addAttribute(QUOTE_TEXT_ATTRIBUTE_NAME, quoteText);

        return "quote";
    }
}
