package dk.testhuset.web.domain;

import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class WebVersion {
    public static final String UNKNOWN = "UNKNOWN";

    public String getApplicationVersion() {
        String version = UNKNOWN;
        Class<? extends WebVersion> aClass = this.getClass();
        Package aPackage = aClass.getPackage();
        String implementationVersion = aPackage.getImplementationVersion();

        if (!StringUtils.isEmpty(implementationVersion)) {
            version = implementationVersion;
        }

        return version;
    }
}
