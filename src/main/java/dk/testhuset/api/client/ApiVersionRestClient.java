package dk.testhuset.api.client;

import dk.samples.api.dto.VersionRestResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

@Service
public class ApiVersionRestClient implements ApiVersionClient {
    private final Logger log = Logger.getLogger(this.getClass());

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private Environment env;

    @Override
    public VersionRestResponse loadApiVersion() {
        String urlToService = this.env.getProperty("version.service.url");
        VersionRestResponse versionRestResponse = new VersionRestResponse(){
            @Override
            public String getApplicationVersion() {
                return "Unknown";
            }

            @Override
            public String getDatabaseVersion() {
                return "Unknown";
            }
        };

        try {
            versionRestResponse = this.restTemplate.getForObject(urlToService, VersionRestResponse.class);
        } catch (RestClientException e) {
            log.warn("Could not retrieve version from "+urlToService, e);
        }

        return versionRestResponse;
    }
}