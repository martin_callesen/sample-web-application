package dk.testhuset.api.client;

import dk.samples.api.dto.VersionRestResponse;

public interface ApiVersionClient {
    VersionRestResponse loadApiVersion();
}
