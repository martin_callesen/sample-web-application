package dk.testhuset.api.client;

import dk.samples.api.dto.QuoteRestResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

@Service
public class ApiQuoteRestClient implements ApiQuoteClient {
    private final Logger log = Logger.getLogger(this.getClass());

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private Environment env;

    @Override
    public String getQuote() {
        String urlToQuoteService = this.env.getProperty("quote.service.url");
        QuoteRestResponse resp = null;

        try {
            resp = this.restTemplate.getForObject(urlToQuoteService, QuoteRestResponse.class);
        } catch (RestClientException e) {
            log.warn("Could not retrieve quote from "+urlToQuoteService, e);
        }

        return resp.getQuote();
    }
}