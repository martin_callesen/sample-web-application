package dk.testhuset.api.client;

public interface ApiQuoteClient {
    String getQuote();
}
