FROM maven:3.2-jdk-8

VOLUME /tmp
COPY target/sample-web.jar sample-web.jar
RUN bash -c 'touch /sample-web.jar'
ENTRYPOINT ["java","-Xdebug", "-Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=5005", "-Djava.security.egd=file:/dev/./urandom","-jar","/sample-web.jar"]